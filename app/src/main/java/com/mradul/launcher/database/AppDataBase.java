package com.mradul.launcher.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mradul.launcher.model.ApplicationInfo;

import java.util.ArrayList;

/**
 * Created by ShekharKG on 5/1/2015.
 */
public class AppDataBase {

  private AppQueries appQueries;
  private SQLiteDatabase sqLiteDatabase;
  private static AppDataBase appDataBase;

  /**
   * Constructor
   */
  private AppDataBase(Context context) {
    appQueries = new AppQueries(context);
    sqLiteDatabase = appQueries.getWritableDatabase();
  }

  /**
   * Static class to make AppDataBase singleton class
   */
  public static AppDataBase singleton(Context context) {
    if (appDataBase == null)
      appDataBase = new AppDataBase(context);
    return appDataBase;
  }


  /**
   * Add a single & unique app details into launcher table
   */
  public long addAppData(String appName, String packageName) {

    //Checks whether the details is already present in database or not
    Cursor cursor = sqLiteDatabase.query(AppQueries.TABLE_LAUNCHER,
        new String[]{AppQueries.APP_NAME, AppQueries.PACKAGE_NAME},
        AppQueries.APP_NAME + " = ? AND " + AppQueries.PACKAGE_NAME + " = ?",
        new String[]{appName, packageName}, null, null, null);

    //If already present then return
    if (cursor.getCount() > 0)
      return -1;

    //Else insert details into table
    ContentValues contentValues = new ContentValues();
    contentValues.put(AppQueries.APP_NAME, appName);
    contentValues.put(AppQueries.PACKAGE_NAME, packageName);
    return sqLiteDatabase.insert(AppQueries.TABLE_LAUNCHER, null, contentValues);
  }

  /**
   * Get all app data from launcher table
   */
  public ArrayList<ApplicationInfo> getAllData() {

    //Get all details in cursor object
    Cursor cursor = sqLiteDatabase.query(AppQueries.TABLE_LAUNCHER,
        new String[]{AppQueries.UID, AppQueries.APP_NAME, AppQueries.PACKAGE_NAME}
        , null, null, null, null, AppQueries.APP_NAME, null);

    //Creating ApplicationInfo object from cursor data
    ArrayList<ApplicationInfo> application_info = new ArrayList<ApplicationInfo>();
    while (cursor.moveToNext()) {
      ApplicationInfo singleData = new ApplicationInfo(cursor.getInt(0),
          cursor.getString(1), cursor.getString(2));
      application_info.add(singleData);
    }
    return application_info;
  }


  /**
   * Add data to home launcher table
   */
  public long addHomeAppData(String appName, String packageName) {

    //Checks whether the details is already present in database or not
    Cursor cursor = sqLiteDatabase.query(AppQueries.TABLE_HOME_LAUNCHER,
        new String[]{AppQueries.APP_NAME, AppQueries.PACKAGE_NAME},
        AppQueries.APP_NAME + " = ? AND " + AppQueries.PACKAGE_NAME + " = ?",
        new String[]{appName, packageName}, null, null, null);

    //If already present then return
    if (cursor.getCount() > 0)
      return -55;

    //Else insert details into table
    ContentValues contentValues = new ContentValues();
    contentValues.put(AppQueries.APP_NAME, appName);
    contentValues.put(AppQueries.PACKAGE_NAME, packageName);
    return sqLiteDatabase.insert(AppQueries.TABLE_HOME_LAUNCHER, null, contentValues);
  }

  /**
   * Get all app data from home launcher table
   */
  public ArrayList<ApplicationInfo> getAllHomeData() {

    //Get all details in cursor object
    Cursor cursor = sqLiteDatabase.query(AppQueries.TABLE_HOME_LAUNCHER,
        new String[]{AppQueries.UID, AppQueries.APP_NAME, AppQueries.PACKAGE_NAME}
        , null, null, null, null, AppQueries.APP_NAME, null);

    //Creating ApplicationInfo object from cursor data
    ArrayList<ApplicationInfo> application_info2 = new ArrayList<ApplicationInfo>();
    int i = 0;
    while (cursor.moveToNext()) {
      if (i < 10) {
        i++;
        ApplicationInfo singleData = new ApplicationInfo(cursor.getInt(0),
            cursor.getString(1), cursor.getString(2));
        application_info2.add(singleData);
      }
    }
    return application_info2;
  }

  /**
   * Update app details in home launcher table
   */
  public void updateData(String wVale, String data1, String data2) {
    ContentValues values = new ContentValues();
    values.put(AppQueries.APP_NAME, data1);
    values.put(AppQueries.PACKAGE_NAME, data2);
    sqLiteDatabase.update(AppQueries.TABLE_HOME_LAUNCHER, values, AppQueries.UID + "=?", new String[]{wVale});
  }


  /**
   * Delete app data from home launcher and launcher table in case of app uninstalling
   */
  public void deleteData(String appName, String packageName) {
    sqLiteDatabase.delete(AppQueries.TABLE_LAUNCHER,
        AppQueries.APP_NAME + " = ? AND " + AppQueries.PACKAGE_NAME + " = ?",
        new String[]{appName, packageName});

    sqLiteDatabase.delete(AppQueries.TABLE_HOME_LAUNCHER,
        AppQueries.APP_NAME + " = ? AND " + AppQueries.PACKAGE_NAME + " = ?",
        new String[]{appName, packageName});
  }

  /**
   * SQLiteDbHelper class
   */
  static public class AppQueries extends SQLiteOpenHelper {

    /**
     * Database name
     */
    private static final String DataBaseName = "dbLauncher";

    /**
     * Table names
     */
    private static final String TABLE_LAUNCHER = "tableLauncher";
    private static final String TABLE_HOME_LAUNCHER = "tableHomeLauncher";
    private static final String TABLE_PREFERENCES = "tablePreferences";

    /**
     * Database version
     */
    private static final int DATA_BASE_VERSION = 1;

    /**
     * Column names
     */
    private static final String UID = "_id";
    private static final String APP_NAME = "AppName";
    private static final String PACKAGE_NAME = "PackageName";
    private static final String HOME_BG_IMAGE = "homeBgImage";
    private static final String LAUNCHER_IMAGE_SELECTED_SHAPE = "launcherImageSelectedShape";

    /**
     * Create table queries
     */
    private static final String CREATE_TABLE_LAUNCHER = "CREATE TABLE " + TABLE_LAUNCHER + " (" + UID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + APP_NAME + " VARCHAR(255), " + PACKAGE_NAME + " VARCHAR(255));";

    private static final String CREATE_TABLE_HOME_LAUNCHER = "CREATE TABLE " + TABLE_HOME_LAUNCHER + " (" + UID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + APP_NAME + " VARCHAR(255), " + PACKAGE_NAME + " VARCHAR(255));";

    private static final String CREATE_TABLE_PREFERENCES = "CREATE TABLE " + TABLE_PREFERENCES + " (" + UID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + HOME_BG_IMAGE + " TEXT, " + LAUNCHER_IMAGE_SELECTED_SHAPE + " INTEGER);";

    /**
     * Drop table queries
     */
    private final String DROP_TABLE_LAUNCHER = "DROP TABLE IF EXISTS " + AppQueries.TABLE_LAUNCHER;
    private final String DROP_TABLE_HOME_LAUNCHER = "DROP TABLE IF EXISTS " + AppQueries.TABLE_LAUNCHER;
    private final String DROP_TABLE_PREFERENCES = "DROP TABLE IF EXISTS " + AppQueries.TABLE_PREFERENCES;

    /**
     * Constructor
     */
    public AppQueries(Context context) {
      super(context, DataBaseName, null, DATA_BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
      sqLiteDatabase.execSQL(CREATE_TABLE_LAUNCHER);
      sqLiteDatabase.execSQL(CREATE_TABLE_HOME_LAUNCHER);
      sqLiteDatabase.execSQL(CREATE_TABLE_PREFERENCES);
    }

    @Override

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
      sqLiteDatabase.execSQL(DROP_TABLE_LAUNCHER);
      sqLiteDatabase.execSQL(DROP_TABLE_HOME_LAUNCHER);
      sqLiteDatabase.execSQL(DROP_TABLE_PREFERENCES);
      onCreate(sqLiteDatabase);
    }
  }
}