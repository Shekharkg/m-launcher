package com.mradul.launcher.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mradul.launcher.async.UpdateAppDatabase;

/**
 * Created by ShekharKG on 5/1/2015.
 */
public class AppInstallOrRemove extends BroadcastReceiver {

  private final String TAG = "M LAUNCHER";

  @Override
  public void onReceive(Context context, Intent intent) {

    // Run UpdateAppDatabase for new app installation or removal
    new UpdateAppDatabase(context).execute();
    Log.e(TAG, "AppInstallOrRemove onReceive");

  }
}