package com.mradul.launcher.async;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

/**
 * Created by ShekharKG on 5/1/2015.
 */
public class UpdateAppDatabase extends AsyncTask<Void, Void, String> {

  private final String TAG = "M LAUNCHER";
  private Context context;

  public UpdateAppDatabase(Context context) {
    this.context = context;
  }

  @Override
  protected String doInBackground(Void... params) {
    Log.d(TAG, "UpdateAppDatabase doInBackGround");

    //Get PackageInfo list of installed apps
    PackageManager packageManager = context.getPackageManager();
    List<PackageInfo> listAppInfo = packageManager.getInstalledPackages(PackageManager.GET_ACTIVITIES);

    //Iterating PackageInfo for package name
    for (PackageInfo packageInfo : listAppInfo) {
      //Discarding android's default apps with package name null
      if (context.getPackageManager().getLaunchIntentForPackage(packageInfo.packageName) != null) {
        Log.i(TAG, "Package name : " + packageInfo.packageName + ", App name : " + packageInfo.applicationInfo.loadLabel(packageManager).toString());
      }
    }

    return null;
  }
}
