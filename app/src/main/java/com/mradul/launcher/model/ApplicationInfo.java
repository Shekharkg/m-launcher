package com.mradul.launcher.model;

/**
 * Created by ShekharKG on 5/3/2015.
 */
public class ApplicationInfo {

  private int id;
  private String appName;
  private String packageName;

  public ApplicationInfo(int id, String appName, String packageName) {
    this.id = id;
    this.appName = appName;
    this.packageName = packageName;
  }

  public int getId() {
    return id;
  }

  public String getAppName() {
    return appName;
  }

  public String getPackageName() {
    return packageName;
  }
}
