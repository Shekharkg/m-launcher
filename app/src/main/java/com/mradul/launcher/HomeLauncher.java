package com.mradul.launcher;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.github.siyamed.shapeimageview.ShaderImageView;
import com.mradul.launcher.reside_menu.ResideMenu;
import com.mradul.launcher.reside_menu.ResideMenuItem;

public class HomeLauncher extends Activity implements View.OnClickListener {

  private ResideMenu resideMenu;
  private ResideMenuItem rounded;
  private ResideMenuItem circular;
  private ResideMenuItem diamond;
  private ResideMenuItem pentagon;
  private ResideMenuItem hexagon;
  private ResideMenuItem octagon;
  private ResideMenuItem star;
  private ResideMenuItem heart;

  private Bitmap bitmap = null;
  private Bundle bundle;
  private int layoutIndex;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    switch (layoutIndex) {
      case 0:
        setContentView(R.layout.rounded);
        break;
      case 1:
        setContentView(R.layout.circular);
        break;
      case 2:
        setContentView(R.layout.diamond);
        break;
      case 3:
        setContentView(R.layout.pentagon);
        break;
      case 4:
        setContentView(R.layout.hexagon);
        break;
      case 5:
        setContentView(R.layout.octagon);
        break;
      case 6:
        setContentView(R.layout.star);
        break;
      case 7:
        setContentView(R.layout.heart);
        break;
    }

    bundle = savedInstanceState;

    FrameLayout mainFrame = (FrameLayout) findViewById(R.id.mainFrame);
    mainFrame.setBackground(getResources().getDrawable(R.drawable.bg));

    // Get default image in bitmap only once
    if (bitmap == null)
      bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.skg);


    // Initialize all image views
    ShaderImageView img1 = (ShaderImageView) findViewById(R.id.image1);
    ShaderImageView img2 = (ShaderImageView) findViewById(R.id.image2);
    ShaderImageView img3 = (ShaderImageView) findViewById(R.id.image3);
    ShaderImageView img4 = (ShaderImageView) findViewById(R.id.image4);
    ShaderImageView img5 = (ShaderImageView) findViewById(R.id.image5);
    ShaderImageView img6 = (ShaderImageView) findViewById(R.id.image6);
    ShaderImageView img7 = (ShaderImageView) findViewById(R.id.image7);
    ShaderImageView img8 = (ShaderImageView) findViewById(R.id.image8);

    //set bitmap on image views
    img1.setImageBitmap(bitmap);
    img2.setImageBitmap(bitmap);
    img3.setImageBitmap(bitmap);
    img4.setImageBitmap(bitmap);
    img5.setImageBitmap(bitmap);
    img6.setImageBitmap(bitmap);
    img7.setImageBitmap(bitmap);
    img8.setImageBitmap(bitmap);

    // attach to current activity;
    resideMenu = new ResideMenu(this);
    resideMenu.setBackground(R.drawable.menu_background);
    resideMenu.attachToActivity(this);
    resideMenu.setMenuListener(menuListener);
    //valid scale factor is between 0.0f and 1.0f. left menu width is 150dip.
    resideMenu.setScaleValue(0.6f);
    resideMenu.setOnClickListener(this);

    // create menu items;
    rounded = new ResideMenuItem(this, R.drawable.icon_home, "Rounded");
    circular = new ResideMenuItem(this, R.drawable.icon_profile, "Circular");
    diamond = new ResideMenuItem(this, R.drawable.icon_calendar, "Diamond");
    pentagon = new ResideMenuItem(this, R.drawable.icon_settings, "Pentagon");
    hexagon = new ResideMenuItem(this, R.drawable.icon_home, "Hexagon");
    octagon = new ResideMenuItem(this, R.drawable.icon_profile, "Octagon");
    star = new ResideMenuItem(this, R.drawable.icon_calendar, "Star");
    heart = new ResideMenuItem(this, R.drawable.icon_settings, "Heart");


    resideMenu.addMenuItem(rounded, ResideMenu.DIRECTION_LEFT);
    resideMenu.addMenuItem(circular, ResideMenu.DIRECTION_LEFT);
    resideMenu.addMenuItem(diamond, ResideMenu.DIRECTION_LEFT);
    resideMenu.addMenuItem(pentagon, ResideMenu.DIRECTION_LEFT);
    resideMenu.addMenuItem(hexagon, ResideMenu.DIRECTION_RIGHT);
    resideMenu.addMenuItem(octagon, ResideMenu.DIRECTION_RIGHT);
    resideMenu.addMenuItem(star, ResideMenu.DIRECTION_RIGHT);
    resideMenu.addMenuItem(heart, ResideMenu.DIRECTION_RIGHT);

    rounded.setOnClickListener(this);
    circular.setOnClickListener(this);
    diamond.setOnClickListener(this);
    pentagon.setOnClickListener(this);
    hexagon.setOnClickListener(this);
    octagon.setOnClickListener(this);
    star.setOnClickListener(this);
    heart.setOnClickListener(this);
  }

  @Override
  public boolean dispatchTouchEvent(MotionEvent ev) {
    return resideMenu.dispatchTouchEvent(ev);
  }

  /**
   * Lister for ResideMenu open close listener
   */
  private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
    @Override
    public void openMenu() {
    }

    @Override
    public void closeMenu() {
    }
  };


  @Override
  public void onClick(View v) {

    resideMenu.closeMenu();

    if (v == rounded) {
      layoutIndex = 0;
    } else if (v == circular) {
      layoutIndex = 1;
    } else if (v == diamond) {
      layoutIndex = 2;
    } else if (v == pentagon) {
      layoutIndex = 3;
    } else if (v == hexagon) {
      layoutIndex = 4;
    } else if (v == octagon) {
      layoutIndex = 5;
    } else if (v == star) {
      layoutIndex = 6;
    } else if (v == heart) {
      layoutIndex = 7;
    }

    if(v != resideMenu)
      onCreate(bundle);

  }
}
